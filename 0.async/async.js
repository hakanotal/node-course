// Example Dataset
const posts = [
    { title: "post 1", body: "lorem" },
    { title: "post 2", body: "ipsum" },
]

function getPosts() {
    setTimeout(() => {
        console.log("Posts:")
        posts.forEach((p) => console.log(p))
    }, 1000)
}

// Callbacks
function createPosts(post, callback) {
    setTimeout(() => {
        posts.push(post)
        callback()
    }, 1000)
}

createPosts({ title: "post 3", body: "callback" }, getPosts)

// Promise
function createPosts2(post) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            posts.push(post)
            resolve()
        }, 1000)
    })
}

createPosts2({ title: "post 4", body: "promise" })
    .then(getPosts())
    .catch((err) => console.log(err))

// Async / Await
async function createPosts3(post) {
    await createPosts2(post)
    getPosts()
}

createPosts3({ title: "post 5", body: "async await" })
