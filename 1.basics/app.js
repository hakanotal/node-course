// Modules
const sum = require("./sum")

console.log(sum(3, 2))

// Events
const EventEmitter = require("events")
const eventEmitter = new EventEmitter()

eventEmitter.on("tutorial", (a, b) => {
    console.log("event occured: %d", sum(a, b))
})

eventEmitter.emit("tutorial", 4, 7)

// File System
const fs = require("fs")

fs.writeFile("example.txt", "Lorem ipsum dolor sit amet", (err) => {
    err ? console.error(err) : console.log("File created")
})
fs.readFile("example.txt", "utf8", (err, file) => {
    err ? console.error(err) : console.log(file)
})

// Streams
let read = fs.createReadStream("example.txt", "utf8")
read.on("data", (chunk) => console.log(chunk))

// Pipes
const zlib = require("zlib")
const gzip = zlib.createGzip()
let r = fs.createReadStream("example.txt", "utf8")
let w = fs.createWriteStream("example.txt.gz")
r.pipe(gzip).pipe(w)
