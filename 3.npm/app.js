const _ = require("lodash")

let ex = _.fill([1, 2, 3, 4, 5], "banana", 1, 4)

console.log(ex)

/*
dependency versions example:

"module": "4.17.11" => uses 4.17.11 (spesific)
"module": "~4.17.11" => uses 4.17.x (patch)
"module": "^4.17.11" => uses 4.x.x (minor update)

*/
